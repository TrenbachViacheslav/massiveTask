﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace massiveTask
{
    static class Calculation
    {
        public static int[][] Array(int N, int M)
        {
            Random rand = new Random();
            int[][] arr = new int[N][];
            for (int i = 0; i < N; i++)
                arr[i] = new int[M];
            for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++)
                arr[i][j] = rand.Next(0, 1000);

           return arr;
        }
        public static int[][] PrintArray(int N, int M, int [][]arr)
        {
            Console.WriteLine("All array = ");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                    Console.Write( arr[i][j] + "\t");
                Console.WriteLine();
            }
            return arr;
        }

        
        public static int MinElement(int N, int M, int[][] arr)
        {
            int minElement = arr[0][0];
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (minElement > arr[i][j])
                    {
                        minElement = arr[i][j];
                    }
                }
            }
            return minElement;
        }

        public static int MaxElement(int N, int M, int[][] arr)
        {
            int maxElement = arr[0][0];
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (maxElement < arr[i][j])
                    {
                        maxElement = arr[i][j];
                    }
                }
            }
            return maxElement;
        }

       
        public static void SortDesc(int[][] arr )
        {
            Console.WriteLine("Отсортировано построчно");
            foreach (int[] row1 in arr)
            {
                foreach (int number in row1.OrderByDescending(x => x))
                {
                    Console.Write($"{number} \t");
                }
                Console.WriteLine();
            }
        }

        public static void SortAsc(int[][] arr)
        {
            Console.WriteLine("Отсортировано построчно");
            foreach (int[] row in arr)
            {
                foreach (int number in row.OrderBy(x => x))
                {
                    Console.Write($"{number} \t");
                }
                Console.WriteLine();
            }
            
        }

        public static void Summ(int[][] arr)
        {
            int result = 0;
            foreach (int[] row in arr)
            {
                result += row.Sum();
            }
            Console.WriteLine($"Sum is - {result}");
        }
    }
}
