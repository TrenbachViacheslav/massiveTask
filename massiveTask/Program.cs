﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace massiveTask
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int N, M;
                Console.WriteLine("enter size (N) : ");
                N = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                Console.WriteLine("enter size (M) : ");
                M = int.Parse(Console.ReadLine());

                bool dead = false;
                while (!dead)
                {
                    Console.WriteLine("Press 1: to show minimal element from array");
                    Console.WriteLine("Press 2: to show maximal element from array");
                    Console.WriteLine("Press 3: to show sorting in decreasing order");
                    Console.WriteLine("Press 4: to show sort ascending");
                    Console.WriteLine("Press 5: to show all Array");
                    Console.WriteLine("Press 6: to know Sum");
                    Console.WriteLine("Press 7: to Exit");


                    var knop = Console.ReadKey();
                    Console.Clear();
                    switch (knop.KeyChar)
                    {
                        case '1':
                            var minmass = Calculation.Array(N, M);
                            Console.WriteLine("minimal element = {0}", Calculation.MinElement(N, M, minmass));
                            break;
                        case '2':
                            var maxmass = Calculation.Array(N, M);
                            Console.WriteLine("maximal element = {0}", Calculation.MaxElement(N, M, maxmass));
                            break;
                        case '3':
                            var dsortmass = Calculation.Array(N, M);
                            Calculation.SortDesc(dsortmass);
                            break;
                        case '4':
                            var asortmass = Calculation.Array(N, M);
                            Calculation.SortAsc(asortmass);
                            break;
                        case '5':
                            var allmass = Calculation.Array(N, M);
                            Calculation.PrintArray(N, M, allmass);
                            break;
                        case '6':
                            var summass = Calculation.Array(N, M);
                            Calculation.Summ(summass);
                            break;
                        case '7':
                            dead = true;
                            break;


                        default:
                            Console.WriteLine("you press unknown key");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }



           
        }
    }
}
